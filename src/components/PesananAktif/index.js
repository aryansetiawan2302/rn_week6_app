import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity, Dimensions } from 'react-native'
import { IconPesananAktif } from '../../assets'
import { WARNA_ABU, WARNA_UTAMA, WARNA_WARNING } from '../../utils/constant'

const PesananAktif = ({title, status}) => {
    return (
        <TouchableOpacity style={styles.container}>
            <IconPesananAktif />
            <View style={styles.text}>
                <Text style={styles.title}>{title}</Text>
                <Text style={styles.status(status)}>{status}</Text>
            </View>
        </TouchableOpacity>
    )
}

export default PesananAktif

const windowWidth = Dimensions.get('window').width; 
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        padding: 17,
        backgroundColor: 'white',
        flexDirection: 'row',
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,
        elevation: 10,
        marginVertical: windowHeight*0.02,
        alignItems: 'center',
    },
    text: {
        marginLeft: windowWidth*0.02,
    },
    title: {
        fontSize: 18,
        fontFamily: 'TitilliumWeb-SemiBold',
    },
    status: (status) => ({
        fontSize: 14,
        fontFamily: 'TitilliumWeb-Light',
        color: status === 'Sudah selesai' ? WARNA_UTAMA : status === 'Masih dicuci' ? WARNA_WARNING : WARNA_ABU,
    }),
})
