import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
// import { createStackNavigator } from '@react-navigation/stack';
import Router from './router';

// function HomeScreen() {
//   return (
//     <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
//       <Text>Home Screen</Text>
//     </View>
//   );
// }

// const Stack = createStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Router />
    </NavigationContainer>  
    // <NavigationContainer>
    //     <Stack.Navigator>
    //         <Stack.Screen name="Home" component={HomeScreen} />
    //     </Stack.Navigator>
    // </NavigationContainer>
  )
}

export default App

const styles = StyleSheet.create({})
