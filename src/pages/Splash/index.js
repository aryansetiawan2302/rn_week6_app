import React, { useEffect } from 'react'
import { StyleSheet, Text, View, ImageBackground, Image } from 'react-native'
import { SplashBackground, Logo } from '../../assets'

const Splash = ({ navigation }) => {

    useEffect(()=>{ //perintah yg dijalankan pertama kali
        setTimeout ( ()=>{
            navigation.replace('MainApp'); //menuju main app
        }, 3000) // delay 3 detik
    }, [navigation])

    return (
        <ImageBackground source={SplashBackground} style={styles.background}>
            <Image source={Logo} style={styles.logo} />
        </ImageBackground>
        // <View>
        //     <Text>Splash</Text>
        // </View>
    )
}

export default Splash

const styles = StyleSheet.create({
    background: {
        flex: 1, // mengisi keseluruhan screen
        alignItems: 'center',
        justifyContent: 'center',
    },
    logo: {
        width: 222,
        height: 105,
    }
})